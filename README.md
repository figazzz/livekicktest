# LivekickTest

Few notes I want to leave here in order to avoid any confusions

1. I've chosen MVC architecture since this architecture's approach fits technical assessment perfectly. I personally like to use MVVM in most comercial projects but in this case it would look like overthinking
2. I didn't integrate right button at the navigationBar at Movie Grid Controller since I have nothing to show there
3. In the UI you provided there's a label which show Rating of the movie. As far as I understood, I should use a parameter 'adult' from Movie Database API in order to define such rating. Is that correct? The problem is - it doesn't look like it's working properly, since it always has 'false' parameter
4. I've documented certain methods in order to help my reviewer to navigate through the logic of the project.
