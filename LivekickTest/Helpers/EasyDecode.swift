//
//  EasyDecode.swift
//  LivekickTest
//
//  Created by Eugene Alexeev on 04/05/2019.
//  Copyright © 2019 user. All rights reserved.
//

import Foundation

final class EasyDecode
{
    static func create<T: Codable>(from json: Any, dateFormat: String? = nil) -> T?
    {
        let decoder = JSONDecoder()
        if let format = dateFormat {
            let formatter = DateFormatter()
            formatter.dateFormat = format
            decoder.dateDecodingStrategy = .formatted(formatter)
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            let obj = try decoder.decode(T.self, from: data)
            return obj
        } catch {
            print("EasyDecode.create(): decoding error = \(error)")
            return nil
        }
    }
}
