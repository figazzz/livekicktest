//
//  UIColor+customColors.swift
//  LivekickTest
//
//  Created by Eugene Alexeev on 07/05/2019.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

extension UIColor {
    static let middleGray = UIColor(red: 85.0/255.0, green: 97.0/255.0, blue: 125.0/255.0, alpha: 1.0)
}
