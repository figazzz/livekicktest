//
//  MovieCollectionViewCell.swift
//  LivekickTest
//
//  Created by Eugene Alexeev on 06/05/2019.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

final class MovieCollectionViewCell: UICollectionViewCell
{
    //MARK: IBOutlets
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var posterView: UIView!
    @IBOutlet weak var posterImgView: UIImageView!
    
    //MARK: Life cycle
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        contentView.clipsToBounds = true
        contentView.layer.cornerRadius = 10.0
    }
    
    //MARK: Public methods
    func configurate(with movieInfo: MovieInfo)
    {
        setLoadingState(true)
        NetworkManager.shared.loadPosterImage(for: movieInfo, preferredWidth: Float(self.frame.size.width)) { [weak self](img) in
            guard let img = img else {
                return
            }
            
            self?.setLoadingState(false)
            self?.posterImgView.image = img
        }
    }
    
    //MARK: Private methods
    private func setLoadingState(_ isLoading: Bool)
    {
        loadingView.isHidden = !isLoading
        posterView.isHidden = isLoading
    }
}
