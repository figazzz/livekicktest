//
//  MovieFooterCollectionReusableView.swift
//  LivekickTest
//
//  Created by Eugene Alexeev on 04/05/2019.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

protocol MovieFooterCollectionReusableViewDelegate: class
{
    func loadMoreDidPress()
}

final class MovieFooterCollectionReusableView: UICollectionReusableView
{
    weak var delegate: MovieFooterCollectionReusableViewDelegate?
    
    @IBOutlet weak var btnLoadMore: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        btnLoadMore.layer.cornerRadius = 5.0
        btnLoadMore.layer.borderWidth = 1.0
        btnLoadMore.layer.borderColor = UIColor.middleGray.cgColor
        btnLoadMore.setTitleColor(.middleGray, for: .normal)
    }
    
    @IBAction func loadMoreButtonAction(_ sender: UIButton)
    {
        delegate?.loadMoreDidPress()
    }
}
