//
//  NetworkManager.swift
//  LivekickTest
//
//  Created by Eugene Alexeev on 01/05/2019.
//  Copyright © 2019 user. All rights reserved.
//

import Foundation
import Alamofire

// MARK: - Urls
private final class Urls
{
    private let k_BASE: URL = URL(string: "https://api.themoviedb.org/3")!
    
    var getConfigurationUrl: URL {
        return k_BASE.appendingPathComponent("configuration")
    }
    
    var getNowPlayingMoviesUrl: URL {
        return k_BASE.appendingPathComponent("movie/now_playing")
    }
}

// MARK: - NetworkManager
final class NetworkManager
{
    // MARK: Typealiases
    typealias Callback = () -> ()
    typealias SuccessCallback = (_ isSuccess: Bool) -> ()
    typealias MoviesCallback = (_ movies: [MovieInfo]) -> ()
    typealias ImageCallback = (_ img: UIImage?) -> ()
    
    typealias FailureCallback = (_ msg: String) -> ()
    
    // MARK: 'shared' for Singleton implementation
    static let shared = NetworkManager()
    
    // MARK: JSON keys
    private let key_API_KEY = "api_key"
    private let key_PAGE = "page"
    private let key_IMAGES = "images"

    // MARK: Default JSON parameters
    private let param_API_KEY = "ebea8cfca72fdff8d2624ad7bbf78e4c"
    
    // MARK: Private variables
    private let urls = Urls()
    private let cacheManager = CacheManager()
    
    private let sessionManager: SessionManager
    
    private(set) var currentConfig: MovieDatabaseConfiguration?
    
    // MARK: private init. NetworkManager is a singleton and it should be used only through 'shared'
    private init()
    {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 60
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    /// Loads config from Movie Database. Config contains a url for fetching images and other necessary parameters like posterSizes, etc
    ///
    /// - Parameters:
    ///   - success: executes in case of success
    ///   - failure: executes in case of failure
    func getConfiguration(success: @escaping Callback, failure: @escaping FailureCallback)
    {
        let _ = sessionManager.request(urls.getConfigurationUrl,
                                       method: .get,
                                       parameters: [key_API_KEY: param_API_KEY],
                                       encoding: URLEncoding.default,
                                       headers: nil)
            .validate()
            .responseJSON { [weak self] (resp) in
                switch resp.result {
                case .success(let value):
                    guard let json = value as? [String: Any],
                        let imagesDict = json["images"] as? [String: Any],
                        let config: MovieDatabaseConfiguration = EasyDecode.create(from: imagesDict) else {
                            failure("Invalid format!")  //here can be more meaningful message
                            return
                    }
                    
                    self?.currentConfig = config
                    success()
                    
                case .failure(let err):
                    print(err.localizedDescription)
                    failure(err.localizedDescription)
                }
        }
    }
    
    /// Fetches movies which fall under the catefory of 'Now playing'. Supports pagination.
    ///
    /// - Parameters:
    ///   - page: number of a page
    ///   - success: executes in case of success
    ///   - failure: executes in case of failure
    func getNowPlayingMovies(page: Int = 1, success: @escaping MoviesCallback, failure: @escaping FailureCallback)
    {
        let _ = sessionManager.request(urls.getNowPlayingMoviesUrl,
                                       method: .get,
                                       parameters: [key_API_KEY: param_API_KEY,
                                                    key_PAGE: page],
                                       encoding: URLEncoding.default,
                                       headers: nil)
            .validate()
            .responseJSON { (resp) in
                
                switch resp.result {
                case .success(let value):
                    guard let json = value as? [String: Any],
                        let results = json["results"] as? [Any] else {
                            failure("Invalid format!") //here can be more meaningful message
                            return
                    }
                    
                    var movies: [MovieInfo] = []
                    for r in results {
                        guard let movie: MovieInfo = EasyDecode.create(from: r, dateFormat: "YYYY-MM-DD") else {
                            continue
                        }
                        
                        movies.append(movie)
                    }
                    
                    success(movies)
                    
                case .failure(let err):
                    failure(err.localizedDescription)
                }
        }
    }
    
    /// Loads a poster image of the movie.
    ///
    /// - Parameters:
    ///   - movie: movie which poster needs to be fetches
    ///   - preferredWidth: width of the poster which is preferable to have
    ///   - completion: executes when request is completed. if 'img' is nil then request was failed
    func loadPosterImage(for movie: MovieInfo, preferredWidth: Float, completion: @escaping ImageCallback)
    {
        /// Downloads an image from the url address
        ///
        /// - Parameters:
        ///   - url: address of an image
        ///   - completion: executes when request is completed. If 'img' is nil then request was failed
        func downloadImage(from url: URL, completion: @escaping ImageCallback)
        {
            let _ = sessionManager.request(url,
                                           method: .get,
                                           parameters: nil,
                                           encoding: JSONEncoding.default,
                                           headers: nil)
                .validate()
                .responseData { [weak self](resp) in
                    
                    switch resp.result {
                    case .success(let value):
                        guard let img = UIImage(data: value) else {
                            completion(nil)
                            return
                        }
                        
                        completion(img)
                        self?.cacheManager.storeImage(response: resp.response!, data: value, from: url)
                        
                    case .failure:
                        completion(nil)
                    }
            }
        }
        
        //Start of the method's body
        
        guard let config = currentConfig else {
            print("You should load config first. Call getConfiguration() method")
            completion(nil)
            return
        }
        
        let imgUrl = config.posterUrl(for: preferredWidth).appendingPathComponent(movie.posterPath)
        
        cacheManager.fetchImage(from: imgUrl) { (img) in
            if let img = img {
                completion(img)
            } else {
                downloadImage(from: imgUrl, completion: completion)
            }
        }
    }
}
