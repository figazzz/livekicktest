//
//  CacheManager.swift
//  LivekickTest
//
//  Created by Eugene Alexeev on 06/05/2019.
//  Copyright © 2019 user. All rights reserved.
//

import Foundation
import UIKit

final class CacheManager
{
    /// Checks if there's an image stored in URLCache for 'url'
    ///
    /// - Parameters:
    ///   - url: 'url' for feching an image
    ///   - completion: executes at completion of the operation. 'img' is nil if nothing has been found
    func fetchImage(from url: URL, completion: @escaping NetworkManager.ImageCallback)
    {
        let cache = URLCache.shared
        let req = URLRequest(url: url)
        
        DispatchQueue.global(qos: .userInitiated).async {
            guard let data = cache.cachedResponse(for: req)?.data, let image = UIImage(data: data) else {
                
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            DispatchQueue.main.async {
                completion(image)
            }
        }
    }
    
    /// Stores image with URLResponse to URLCache
    ///
    /// - Parameters:
    ///   - response: URLResponse with an image
    ///   - data: image data
    ///   - url: url which can be used in future as a key
    func storeImage(response: URLResponse, data: Data, from url: URL)
    {
        let cache = URLCache.shared
        let req = URLRequest(url: url)
        
        DispatchQueue.global(qos: .userInitiated).async {
            cache.storeCachedResponse(CachedURLResponse(response: response, data: data), for: req)
        }
    }
}
