//
//  DetailsViewController.swift
//  LivekickTest
//
//  Created by Eugene Alexeev on 06/05/2019.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

final class DetailsViewController: UIViewController
{
    // MARK: IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var backgroundView: UIImageView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var posterImgView: UIImageView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: Constants
    private let k_RELEASE_DATE_FORMAT = "MMMM dd, YYYY"
    private let k_RESTRICTED = "R"
    private let k_GENERAL_AUDIENCE = "G"
    
    // MARK: Public properties
    weak var movie: MovieInfo? = nil
    
    // MARK: Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()

        configurateNavigationBar()
        configurateTexts()
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width, height: contentView.frame.size.height)
        
        configurateBackground()
        configuratePosterImage()
    }
    
    // MARK: Private methods
    private func configurateNavigationBar()
    {
        self.title = movie?.title ?? ""
        navigationController?.navigationBar.tintColor = .middleGray
    }
    
    private func configurateBackground()
    {
        let blurEffect = UIBlurEffect(style: .dark)
        blurView.effect = blurEffect
        
        guard let m = movie else {
            return
        }
        
        NetworkManager.shared.loadPosterImage(for: m, preferredWidth: Float(backgroundView.frame.size.width)) { [weak self](img) in
            self?.backgroundView.image = img
        }
    }
    
    private func configuratePosterImage()
    {
        posterImgView.layer.cornerRadius = 10.0
        
        guard let m = movie else {
            return
        }
        
        NetworkManager.shared.loadPosterImage(for: m, preferredWidth: Float(posterImgView.frame.size.width)) { [weak self](img) in
            self?.posterImgView.image = img
        }
    }
    
    private func configurateTexts()
    {
        guard let m = movie else {
            return
        }
        
        scoreLabel.text = String(describing: m.voteAverage)
        
        ratingLabel.text = m.adult ? k_RESTRICTED : k_GENERAL_AUDIENCE
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = k_RELEASE_DATE_FORMAT
        releaseDateLabel.attributedText = getAdjustedText(dateFormatter.string(from: m.releaseDate))
        
        titleLabel.attributedText = getAdjustedText(m.title)
        descriptionLabel.attributedText = getAdjustedText(m.overview)
    }
    
    private func getAdjustedText(_ text: String) -> NSAttributedString
    {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4.0
        
        let attrString = NSMutableAttributedString(string: text)
        attrString.addAttributes([.paragraphStyle: paragraphStyle,
                                  .foregroundColor: UIColor.white,
                                  .kern: 0.9],
                                 range: NSMakeRange(0, attrString.length))
        
        return attrString
    }
    
}
