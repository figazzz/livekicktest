//
//  MovieViewController.swift
//  LivekickTest
//
//  Created by Eugene Alexeev on 06/05/2019.
//  Copyright © 2019 user. All rights reserved.
//

import UIKit

final class MovieViewController: UIViewController
{
    // MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: Private variables
    private var movies: [MovieInfo] = []
    private var page: Int = 1

    // MARK: Constants
    private let k_CELL_ID = "movieCellIdentifier"
    private let k_FOOTER_ID = "movieFooterIdentifier"
    
    private let k_TITLE = "Latest movies"
    private let k_TITLE_FONT_SIZE: CGFloat = 16.0
    private let k_OFFSET_POINT = CGPoint(x: 75.0, y: 150.0)
    
    // MARK: Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        configurateRefreshControl()
        configurateNavigationBar()
        configurateCollectionView()
        
        loadConfiguration()
        
        let navBar = UINavigationBar()
        view.addSubview(navBar)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        
        guard let visibleIndexPath = collectionView.indexPathForItem(at: CGPoint(x: collectionView.contentOffset.x + k_OFFSET_POINT.x, y: collectionView.contentOffset.y + k_OFFSET_POINT.y)) else {
            return
        }
        
        coordinator.animate(alongsideTransition: { [weak self] (_) in
            self?.collectionView.scrollToItem(at: visibleIndexPath, at: .top, animated: false)
            }, completion: nil)
    }
    
    // MARK: Private methods
    private func configurateRefreshControl()
    {
        collectionView.refreshControl = UIRefreshControl()
        collectionView.refreshControl?.addTarget(self, action: #selector(refreshExecuted),
                                                 for: UIControl.Event.valueChanged)
    }
    
    private func configurateNavigationBar()
    {
        self.title = k_TITLE
     
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: k_TITLE_FONT_SIZE),
                                                                   NSAttributedString.Key.foregroundColor: UIColor.middleGray]
    }
    
    private func configurateCollectionView()
    {
        collectionView.register(UINib(nibName: String(describing: MovieFooterCollectionReusableView.self), bundle: nil),
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: k_FOOTER_ID)
        collectionView.register(UINib(nibName: String(describing: MovieCollectionViewCell.self), bundle: nil),
                                forCellWithReuseIdentifier: k_CELL_ID)
    }
    
    private func loadConfiguration(loadMovies: Bool = true)
    {
        NetworkManager.shared.getConfiguration(success: { [weak self] in
            
            if loadMovies {
                self?.loadNowPlayingMovies()
            }
            
        }) { [weak self](msg) in
            self?.showAlertError(msg)
        }
    }
    
    private func loadNowPlayingMovies()
    {
        NetworkManager.shared.getNowPlayingMovies(page: page, success: { [weak self](movies) in
            
            self?.page += 1
            self?.movies.append(contentsOf: movies)
            self?.showNewMovies(movies)
        }) { [weak self](msg) in
            
            self?.moviesUpdateDidError(msg)
        }
    }
    
    private func showNewMovies(_ newMovies: [MovieInfo])
    {
        collectionView.performBatchUpdates({ [weak self] in
            
            guard let strSelf = self else {
                return
            }
            
            let startId = strSelf.movies.count - newMovies.count
            let indexPaths = newMovies.enumerated().map({ (i, m) -> IndexPath in
                return IndexPath(row: startId + i, section: 0)
            })
            
            collectionView.reloadData()
            collectionView.insertItems(at: indexPaths)
            
            }, completion: nil)
    }
    
    private func moviesUpdateDidError(_ msg: String) {
        showAlertError(msg)
    }
    
    @objc private func refreshExecuted()
    {
        collectionView.refreshControl?.endRefreshing()
        
        page = 1
        movies = []
        collectionView.reloadData()
        loadNowPlayingMovies()
    }
    
    private func showAlertError(_ msg: String)
    {
        let alertVC = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alertVC, animated: true, completion: nil)
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension MovieViewController: UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: k_CELL_ID, for: indexPath) as! MovieCollectionViewCell
        cell.configurate(with: movies[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        switch kind {
        case UICollectionView.elementKindSectionFooter:
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: k_FOOTER_ID, for: indexPath) as! MovieFooterCollectionReusableView
            footer.delegate = self
            footer.isHidden = movies.isEmpty
            
            return footer
        default:
            break
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let backItem = UIBarButtonItem()
        backItem.title = "back"
        navigationItem.backBarButtonItem = backItem
        
        let vc = DetailsViewController(nibName: String(describing: DetailsViewController.self), bundle: nil)
        vc.movie = movies[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - MovieFooterCollectionReusableViewDelegate
extension MovieViewController: MovieFooterCollectionReusableViewDelegate
{
    func loadMoreDidPress() {
        loadNowPlayingMovies()
    }
}
