//
//  MovieInfo.swift
//  LivekickTest
//
//  Created by Eugene Alexeev on 04/05/2019.
//  Copyright © 2019 user. All rights reserved.
//

import Foundation

final class MovieInfo: Codable
{
    private enum CodingKeys: String, CodingKey
    {
        case identifier = "id"
        case posterPath = "poster_path"
        case title = "title"
        case overview = "overview"
        case voteAverage = "vote_average"
        case releaseDate = "release_date"
        case adult = "adult"
    }
    
    let identifier: Int
    let adult: Bool
    let posterPath: String
    let title: String
    let overview: String
    let voteAverage: Float
    let releaseDate: Date
}
