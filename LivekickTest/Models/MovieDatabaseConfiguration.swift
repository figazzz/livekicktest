//
//  MovieDatabaseConfiguration.swift
//  LivekickTest
//
//  Created by Eugene Alexeev on 04/05/2019.
//  Copyright © 2019 user. All rights reserved.
//

import Foundation
import CoreGraphics

final class MovieDatabaseConfiguration: Codable
{
    /// an enum which helps to organize Poster sizes
    enum PosterSize: String, Codable {
        case w92
        case w154
        case w185
        case w342
        case w500
        case w780
        case original
        
        /// returns a width of the poster which can be fetched by an enum
        var width: Float {
            switch self {
            case .w92:
                return 92.0
            case .w154:
                return 154.0
            case .w185:
                return 185.0
            case .w342:
                return 342.0
            case .w500:
                return 500.0
            case .w780:
                return 780.0
            default:
                return 1024.0
            }
        }
    }
    
    private enum CodingKeys: String, CodingKey {
        case baseUrl = "base_url"
        case secureBaseUrl = "secure_base_url"
        case posterSizes = "poster_sizes"
    }
    
    let baseUrl: URL
    let secureBaseUrl: URL
    let posterSizes: [PosterSize]
    
    /// Gives a url which can be used for downloading a poster
    ///
    /// - Parameter width: preferable width of the poster. Method will return a url with the poster which width will be closest to the input.
    /// - Returns: a poster url
    func posterUrl(for width: Float? = nil) -> URL
    {
        guard let orientWidth = width else {
            return baseUrl.appendingPathComponent("original")
        }
        
        var minDiff: Float = 1024.0
        var posterSize: PosterSize = .original
        posterSizes.forEach {
            let diff = fabsf($0.width - orientWidth)
            if diff < minDiff {
                minDiff = diff
                posterSize = $0
            }
        }
        
        return baseUrl.appendingPathComponent(posterSize.rawValue)
    }
}
